package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission;


import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter.IPresenter;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter.PresenterImpl;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.repository.IRepo;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.IBrandsView;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by rajagrawal on 23/04/17.
 */

public class PresenterTest {

    private IBrandsView mockView;

    private IPresenter<IBrandsView> presenter;

    private IRepo<Brands, IPresenter<IBrandsView>> mockRepository;

    @Before
    public void setup(){

        mockRepository = Mockito.mock(IRepo.class);

        mockView = mock(IBrandsView.class);

        presenter = new PresenterImpl(mockRepository);

        presenter.setView(mockView);
    }

    @Test
    public void noInteractionShouldTakePlaceIfTheUserIsNull(){

        mockRepository.saveToDatabase(eq(anyString()), presenter);

        verifyZeroInteractions(mockView);
    }

    @Test
    public void validateTheFlowOfAccessingAndUpdatingListFromLocalRepository(){

        when(mockRepository.isLocal()).thenReturn(true);

        presenter.prepareList();

        verify(mockView, times(1)).updateList(ArgumentMatchers.<Brands>anyList());

        verify(mockView, times(1)).dismissProgressIndicator();
    }
}