package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.repository;

/**
 * Created by rajagrawal on 22/04/17.
 */

public interface IRepo<M, P> {

    boolean isLocal();

    int getListItemCount();

    M getBrandByIndex(int position);

    void fetchDataFromAPI(String URL, P presenter);

    void saveToDatabase(String response, P presenter);
}
