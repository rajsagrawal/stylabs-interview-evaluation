package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter;

import java.util.ArrayList;

import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.repository.IRepo;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.utility.Constants;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.IBrandsView;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class PresenterImpl implements IPresenter<IBrandsView> {

    private IBrandsView brandsView;
    private IRepo<Brands, IPresenter<IBrandsView>> repo;
    private ArrayList<Brands> brandsList = new ArrayList<>();

    public PresenterImpl(IRepo<Brands, IPresenter<IBrandsView>> repo){
        this.repo = repo;
    }

    @Override
    public void prepareList() {
        if (repo.isLocal()){
            fetchAListFromLocalRepo();
        }
        else {
            fetchAFreshListFromServer();
        }
    }

    @Override
    public void fetchAListFromLocalRepo() {
        brandsView.showProgressIndicator();
        brandsView.hideFallbackScreen();
        if (brandsList.size()>0) brandsList.clear();
        for (int i = 0; i<repo.getListItemCount(); i++){
            brandsList.add(repo.getBrandByIndex(i));
        }
        brandsView.updateList(brandsList);
        brandsView.dismissProgressIndicator();
    }

    public void fetchAFreshListFromServer() {
        brandsView.showProgressIndicator();
        // The SSL has expired for the https endpoint, so I made a call to the http endpoint instead.
        repo.fetchDataFromAPI(Constants.API.DOMAIN + Constants.API.ASSIGN_TASK, this);
    }

    @Override
    public void forceRefresh() {
        fetchAFreshListFromServer();
    }

    @Override
    public void setView(IBrandsView brandsView) {
        this.brandsView = brandsView;
    }

    @Override
    public void notifyListUpdated() {
        brandsView.notifyTheUser("Populated a more up-to-date list from the server.");
    }

    @Override
    public void listFetchingFailed() {
        brandsView.notifyTheUser("Oh no! Check if the internet connection is working and try again.");
        brandsView.dismissProgressIndicator();
    }
}
