package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class Brands {

    private int manufacturer_id;
    private String manufacturer_data;

    public void setManufacturer_id(int manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }

    public void setManufacturer_data(String manfacturer_data) {
        this.manufacturer_data = manfacturer_data;
    }

    public int getManufacturer_id() {
        return manufacturer_id;
    }

    public String getManufacturer_data() {
        return manufacturer_data;
    }
}
