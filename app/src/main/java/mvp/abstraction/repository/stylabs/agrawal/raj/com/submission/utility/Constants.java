package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.utility;

/**
 * Created by rajagrawal on 23/04/17.
 */

public class Constants {

    public static final class API{
        public static final String DOMAIN = "http://beta.styfi.in";
        public static final String ASSIGN_TASK = "/api/cron/assign_task";
    }
}
