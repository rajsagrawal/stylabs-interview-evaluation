package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view;

import java.util.List;

import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;

/**
 * Created by rajagrawal on 22/04/17.
 */

public interface IBrandsView{

    void showProgressIndicator();

    void dismissProgressIndicator();

    void updateList(List<Brands> array);

    void notifyTheUser(String message);

    void hideFallbackScreen();

}
