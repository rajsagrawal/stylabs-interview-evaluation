package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.R;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.DataHolder>{
    private LayoutInflater mInflater;
    private List<Brands> array = new ArrayList<>();

    public BrandsAdapter(Context context, List<Brands> array) {
        mInflater = LayoutInflater.from(context);
        this.array = array;
    }

    @Override
    public long getItemId(int position) {
        return array.get(position).getManufacturer_id();
    }

    @Override
    public DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.brands_row, parent, false);
        return new DataHolder(view);
    }

    @Override
    public void onBindViewHolder(DataHolder holder, int position) {
        holder.tv_brands_row_mID.setText(holder.tv_brands_row_mID.getContext().getString(R.string.manufacturer_d) + array.get(position).getManufacturer_id());
        holder.tv_brands_row_bID.setText(holder.tv_brands_row_bID.getContext().getString(R.string.brand) + array.get(position).getManufacturer_data());
    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    static class DataHolder extends RecyclerView.ViewHolder {

        private final TextView tv_brands_row_mID;
        private final TextView tv_brands_row_bID;

        DataHolder(View itemView) {
            super(itemView);
            tv_brands_row_mID = (TextView) itemView.findViewById(R.id.tv_brands_row_mID);
            tv_brands_row_bID = (TextView) itemView.findViewById(R.id.tv_brands_row_bID);
        }
    }
}
