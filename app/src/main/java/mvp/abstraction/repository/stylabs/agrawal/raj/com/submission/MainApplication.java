package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission;

import android.app.Application;

import io.realm.Realm;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.utility.Toaster;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Toaster.init(this);
    }
}
