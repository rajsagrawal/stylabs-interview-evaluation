package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class BrandsTable extends RealmObject {

    @PrimaryKey
    private int manufacturer_id;

    private String manufacturer_data;

    public int getManufacturer_id() {
        return manufacturer_id;
    }

    public String getManufacturer_data() {
        return manufacturer_data;
    }

    public void setManufacturer_id(int manufactuer_id) {
        this.manufacturer_id = manufactuer_id;
    }

    public void setManufacturer_data(String manufactuer_data) {
        this.manufacturer_data = manufactuer_data;
    }
}
