package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter;


/**
 * Created by rajagrawal on 22/04/17.
 */

public interface IPresenter<V> {

    void setView(V view);

    void prepareList();

    void fetchAListFromLocalRepo();

    void fetchAFreshListFromServer();

    void forceRefresh();

    void notifyListUpdated();

    void listFetchingFailed();
}
