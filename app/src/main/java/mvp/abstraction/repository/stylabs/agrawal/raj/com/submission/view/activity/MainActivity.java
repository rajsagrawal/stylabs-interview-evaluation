package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.R;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter.IPresenter;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter.PresenterImpl;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.repository.RepoImpl;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.utility.Toaster;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.IBrandsView;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.adapter.BrandsAdapter;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class MainActivity extends AppCompatActivity implements IBrandsView, SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView rv_brands_list;
    private SwipeRefreshLayout swipe_brands_list;
    private TextView tv_brands_empty;

    private BrandsAdapter adapter;

    private IPresenter<IBrandsView> presenter = new PresenterImpl(RepoImpl.getInstance());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseViews();
        attachListeners();
        preparePresenter();
    }

    private void initialiseViews() {
        rv_brands_list = (RecyclerView) findViewById(R.id.rv_brands_list);
        swipe_brands_list = (SwipeRefreshLayout) findViewById(R.id.swipe_brands_list);
        tv_brands_empty = (TextView) findViewById(R.id.tv_brands_empty);
    }

    private void attachListeners() {
        swipe_brands_list.setOnRefreshListener(this);
    }

    private void preparePresenter() {
        presenter.setView(this);
        presenter.prepareList();
    }

    @Override
    public void showProgressIndicator() {
        swipe_brands_list.setRefreshing(true);
    }

    @Override
    public void dismissProgressIndicator() {
        swipe_brands_list.setRefreshing(false);
    }

    @Override
    public void updateList(List<Brands> array) {
        if (adapter==null) {
            adapter = new BrandsAdapter(this, array);
            adapter.setHasStableIds(true);
            rv_brands_list.setLayoutManager(new LinearLayoutManager(this));
            rv_brands_list.setAdapter(adapter);
        }
        else adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyTheUser(String message) {
        Toaster.longToast(message);
    }

    @Override
    public void hideFallbackScreen() {
        tv_brands_empty.setVisibility(View.GONE);
        rv_brands_list.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        presenter.forceRefresh();
    }
}
