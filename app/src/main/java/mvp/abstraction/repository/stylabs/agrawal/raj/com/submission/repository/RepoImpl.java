package mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.repository;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.Sort;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.Brands;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.model.realm.BrandsTable;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.presenter.IPresenter;
import mvp.abstraction.repository.stylabs.agrawal.raj.com.submission.view.IBrandsView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rajagrawal on 22/04/17.
 */

public class RepoImpl implements IRepo<Brands, IPresenter<IBrandsView>> {

    private static final RepoImpl INSTANCE = new RepoImpl();

    private RealmConfiguration config = new RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build();

    private Realm mRealm = Realm.getInstance(config);

    private Handler handler = new Handler(Looper.getMainLooper());

    private RepoImpl(){}

    public static RepoImpl getInstance(){
        return INSTANCE;
    }

    @Override
    public boolean isLocal() {
        return mRealm.where(BrandsTable.class).findAllSorted("manufacturer_id", Sort.ASCENDING).size()>0;
    }

    @Override
    public int getListItemCount() {
        return mRealm.where(BrandsTable.class).findAllSorted("manufacturer_id", Sort.ASCENDING).size();
    }

    public Brands getBrandByIndex(int position) {
        Brands brands = new Brands();
        brands.setManufacturer_id(mRealm.where(BrandsTable.class).findAllSortedAsync("manufacturer_id", Sort.ASCENDING).get(position).getManufacturer_id());
        brands.setManufacturer_data(mRealm.where(BrandsTable.class).findAllSortedAsync("manufacturer_data", Sort.ASCENDING).get(position).getManufacturer_data());
        return brands;
    }

    @Override
    public void fetchDataFromAPI(String URL, final IPresenter<IBrandsView> presenter) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(URL)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("response_api", e.getMessage());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        presenter.listFetchingFailed();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String jsonString = response.body().string();
                Log.d("response_api", jsonString);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        clearRealm();
                        saveToDatabase(jsonString, presenter);
                    }
                });
            }
        });
    }

    @Override
    public void saveToDatabase(String response, IPresenter<IBrandsView> presenter) {

        final JSONArray jsonArrayOfData;

        try {
            jsonArrayOfData = new JSONObject(response).optJSONArray("data");

            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.createOrUpdateAllFromJson(BrandsTable.class, jsonArrayOfData.toString());
                }
            });

            presenter.fetchAListFromLocalRepo();
            presenter.notifyListUpdated();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearRealm() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(BrandsTable.class);
            }
        });
    }
}
